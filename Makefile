all: client server


protoc: 
	@echo "Generating Protobuf Go Files"
	cd proto && protoc --go_out=./station/ --go-grpc_out=./station/ \
		--go_opt=paths=source_relative --go-grpc_opt=paths=source_relative *.proto


server: protoc
	go build -o server gitlab.com/PatrickVienne/grpc-demo/server

client: protoc
	go build -o client gitlab.com/PatrickVienne/grpc-demo/client

